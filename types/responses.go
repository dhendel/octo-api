package types

import "time"

type DeleteResponse struct {
	ID          string `json:"Id"`
	Name        string `json:"Name"`
	Description string `json:"Description"`
	Arguments   struct {
		AdditionalProp1 struct {
		} `json:"additionalProp1"`
		AdditionalProp2 struct {
		} `json:"additionalProp2"`
		AdditionalProp3 struct {
		} `json:"additionalProp3"`
	} `json:"Arguments"`
	State                      string    `json:"State"`
	Completed                  string    `json:"Completed"`
	QueueTime                  time.Time `json:"QueueTime"`
	QueueTimeExpiry            time.Time `json:"QueueTimeExpiry"`
	StartTime                  time.Time `json:"StartTime"`
	LastUpdatedTime            time.Time `json:"LastUpdatedTime"`
	CompletedTime              time.Time `json:"CompletedTime"`
	ServerNode                 string    `json:"ServerNode"`
	Duration                   string    `json:"Duration"`
	ErrorMessage               string    `json:"ErrorMessage"`
	HasBeenPickedUpByProcessor bool      `json:"HasBeenPickedUpByProcessor"`
	IsCompleted                bool      `json:"IsCompleted"`
	FinishedSuccessfully       bool      `json:"FinishedSuccessfully"`
	HasPendingInterruptions    bool      `json:"HasPendingInterruptions"`
	CanRerun                   bool      `json:"CanRerun"`
	HasWarningsOrErrors        bool      `json:"HasWarningsOrErrors"`
	LastModifiedOn             time.Time `json:"LastModifiedOn"`
	LastModifiedBy             string    `json:"LastModifiedBy"`
	Links                      struct {
		AdditionalProp1 string `json:"additionalProp1"`
		AdditionalProp2 string `json:"additionalProp2"`
		AdditionalProp3 string `json:"additionalProp3"`
	} `json:"Links"`
}
