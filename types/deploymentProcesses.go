package types

import "time"

type AllDeploymentProcesses struct {
	ID             string              `json:"Id"`
	ItemType       string              `json:"ItemType"`
	TotalResults   int                 `json:"TotalResults"`
	ItemsPerPage   int                 `json:"ItemsPerPage"`
	NumberOfPages  int                 `json:"NumberOfPages"`
	LastPageNumber int                 `json:"LastPageNumber"`
	Items          []DeploymentProcess `json:"Items"`
}

type DeploymentProcess struct {
	ID        string `json:"Id"`
	ProjectID string `json:"ProjectId"`
	Steps     []struct {
		ID                 string `json:"Id"`
		Name               string `json:"Name"`
		PackageRequirement string `json:"PackageRequirement"`
		Properties         struct {
			AdditionalProp1 struct {
				IsSensitive    bool   `json:"IsSensitive"`
				Value          string `json:"Value"`
				SensitiveValue struct {
					HasValue bool   `json:"HasValue"`
					NewValue string `json:"NewValue"`
				} `json:"SensitiveValue"`
			} `json:"additionalProp1"`
			AdditionalProp2 struct {
				IsSensitive    bool   `json:"IsSensitive"`
				Value          string `json:"Value"`
				SensitiveValue struct {
					HasValue bool   `json:"HasValue"`
					NewValue string `json:"NewValue"`
				} `json:"SensitiveValue"`
			} `json:"additionalProp2"`
			AdditionalProp3 struct {
				IsSensitive    bool   `json:"IsSensitive"`
				Value          string `json:"Value"`
				SensitiveValue struct {
					HasValue bool   `json:"HasValue"`
					NewValue string `json:"NewValue"`
				} `json:"SensitiveValue"`
			} `json:"additionalProp3"`
		} `json:"Properties"`
		Condition    string   `json:"Condition"`
		StartTrigger string   `json:"StartTrigger"`
		Actions      []Action `json:"Actions"`
	} `json:"Steps"`
	Version        int       `json:"Version"`
	LastSnapshotID string    `json:"LastSnapshotId"`
	LastModifiedOn time.Time `json:"LastModifiedOn"`
	LastModifiedBy string    `json:"LastModifiedBy"`
	Links          struct {
		AdditionalProp1 string `json:"additionalProp1"`
		AdditionalProp2 string `json:"additionalProp2"`
		AdditionalProp3 string `json:"additionalProp3"`
	} `json:"Links"`
}

type Action struct {
	ID                            string                 `json:"Id"`
	Name                          string                 `json:"Name"`
	ActionType                    string                 `json:"ActionType"`
	IsDisabled                    bool                   `json:"IsDisabled"`
	CanBeUsedForProjectVersioning bool                   `json:"CanBeUsedForProjectVersioning"`
	IsRequired                    bool                   `json:"IsRequired"`
	WorkerPoolID                  string                 `json:"WorkerPoolId"`
	Environments                  []string               `json:"Environments"`
	ExcludedEnvironments          []string               `json:"ExcludedEnvironments"`
	Channels                      []string               `json:"Channels"`
	TenantTags                    []string               `json:"TenantTags"`
	Properties                    map[string]interface{} `json:"Properties"`
	LastModifiedOn                time.Time              `json:"LastModifiedOn"`
	LastModifiedBy                string                 `json:"LastModifiedBy"`
	Links                         struct {
		AdditionalProp1 string `json:"additionalProp1"`
		AdditionalProp2 string `json:"additionalProp2"`
		AdditionalProp3 string `json:"additionalProp3"`
	} `json:"Links"`
}
