package client

import (
	"fmt"
	"gitlab.com/dhendel/octo-api/types"
)

type Projects struct {
	client *OctoClient
	*types.Project
	req *RequestData
}

func (p *Projects) List() ([]types.Project, error) {
	projects := make([]types.Project, 0)
	p.req.All = true
	p.req.Method = "GET"

	req, err := p.client.newRequest(p.req)

	if err != nil {
		return nil, err
	}

	if err := p.client.Do(req, &projects); err != nil {
		return nil, err
	}

	return projects, nil
}

func (p *Projects) ListByID(projectID string) (*types.Project, error) {
	project := &types.Project{}
	p.req.All = false
	p.req.Method = "GET"
	p.req.ObjectID = projectID

	req, err := p.client.newRequest(p.req)

	if err != nil {
		return nil, err
	}

	if err := p.client.Do(req, project); err != nil {
		return nil, err
	}

	return project, nil
}

func (p *Projects) Update(Role *types.Project) (*types.Project, error) {

	if Role.ID == "" {
		err := fmt.Errorf("project ID must be set to update the project")
		return nil, err
	}

	updateResponse := &types.Project{}
	p.req.All = false
	p.req.Method = "PUT"

	p.req.ObjectID = Role.ID
	p.req.setBody(Role)

	req, err := p.client.newRequest(p.req)

	if err != nil {
		return nil, err
	}

	if err := p.client.Do(req, updateResponse); err != nil {
		return nil, err
	}

	return updateResponse, err
}
func (p *Projects) Create(Role *types.Project) (*types.Project, error) {
	createResponse := &types.Project{}
	p.req.Method = "POST"

	p.req.ObjectID = ""
	p.req.All = false

	p.req.setBody(Role)

	req, err := p.client.newRequest(p.req)

	if err != nil {
		return nil, err
	}

	if err := p.client.Do(req, createResponse); err != nil {
		return nil, err
	}

	return createResponse, nil
}
func (p *Projects) Delete(projectID string) (*types.DeleteResponse, error) {
	deleteResponse := &types.DeleteResponse{}

	if projectID == "" {
		err := fmt.Errorf("project ID must be set to delete the project")
		return nil, err
	}

	p.req.ObjectID = projectID
	p.req.All = false

	req, err := p.client.newRequest(p.req)

	if err != nil {
		return nil, err
	}

	if err := p.client.Do(req, deleteResponse); err != nil {
		return nil, err
	}

	return deleteResponse, nil
}
