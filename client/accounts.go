package client

import (
	"fmt"
	"gitlab.com/dhendel/octo-api/types"
)

type Accounts struct {
	client *OctoClient
	*types.Account
	req *RequestData
}

func (a *Accounts) List() ([]types.Account, error) {
	accounts := make([]types.Account, 0)
	a.req.All = true
	a.req.Method = "GET"

	req, err := a.client.newRequest(a.req)

	if err != nil {
		return nil, err
	}

	if err := a.client.Do(req, &accounts); err != nil {
		return nil, err
	}

	return accounts, nil
}

func (a *Accounts) ListByID(accountID string) (*types.Account, error) {
	account := &types.Account{}
	a.req.All = false
	a.req.Method = "GET"
	a.req.ObjectID = accountID

	req, err := a.client.newRequest(a.req)

	if err != nil {
		return nil, err
	}

	if err := a.client.Do(req, account); err != nil {
		return nil, err
	}

	return account, nil
}

func (a *Accounts) Update(Account *types.Account) (*types.Account, error) {

	if Account.ID == "" {
		err := fmt.Errorf("account ID must be set to update the account")
		return nil, err
	}

	updateResponse := &types.Account{}
	a.req.All = false
	a.req.Method = "PUT"

	a.req.ObjectID = Account.ID
	a.req.setBody(Account)

	req, err := a.client.newRequest(a.req)

	if err != nil {
		return nil, err
	}

	if err := a.client.Do(req, updateResponse); err != nil {
		return nil, err
	}

	return updateResponse, err
}
func (a *Accounts) Create(Account *types.Account) (*types.Account, error) {
	createResponse := &types.Account{}
	a.req.Method = "POST"

	a.req.ObjectID = ""
	a.req.All = false

	a.req.setBody(Account)

	req, err := a.client.newRequest(a.req)

	if err != nil {
		return nil, err
	}

	if err := a.client.Do(req, createResponse); err != nil {
		return nil, err
	}

	return createResponse, nil
}
func (a *Accounts) Delete(accountID string) (*types.DeleteResponse, error) {
	deleteResponse := &types.DeleteResponse{}

	if accountID == "" {
		err := fmt.Errorf("account ID must be set to delete the account")
		return nil, err
	}

	a.req.ObjectID = accountID
	a.req.All = false

	req, err := a.client.newRequest(a.req)

	if err != nil {
		return nil, err
	}

	if err := a.client.Do(req, deleteResponse); err != nil {
		return nil, err
	}

	return deleteResponse, nil
}
